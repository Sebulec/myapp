//
//  DatabaseParser.swift
//  myApp
//
//  Created by Sebastian Kotarski on 19.06.2016.
//  Copyright © 2016 Sebastian Kotarski. All rights reserved.
//

import UIKit

class DatabaseParser: NSObject {
    
    var searchTitle : String?
    var listOfMovies : NSArray!
    
    override init() {
        self.listOfMovies = NSArray()
    }
    
    func makeRequest(searchTitle : String) {
        var json = NSDictionary()
        do {
            let data = getData(searchTitle)
            if (data != nil) {
                json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as! NSDictionary
                unwrapData(json)
            }
        } catch {
            print(error)
        }
    }
    func getData(searchTitle : String) -> NSData? {
        let searchTitle = searchTitle.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())!
        let stringUrl = "http://www.omdbapi.com/?s=\(searchTitle)&page=1&plot=full"
        let url = NSURL(string: stringUrl)
        let data = NSData(contentsOfURL: url!)
        return data
    }
    
    func unwrapData(json : NSDictionary) {
        let response = json.objectForKey("Response") as! String
        if(response == "True") {
            let elements = json.objectForKey("Search") as! NSArray
            let dataArray = NSMutableArray(capacity : elements.count)
            for movie in elements {
                let m = Movie(title: movie.objectForKey("Title") as! String, plot: "", genre: "", posterUrl: movie.objectForKey("Poster") as! String, year: movie.objectForKey("Year") as! String, id: movie.objectForKey("imdbID") as! String)
                m.loadImage()
                dataArray.addObject(m)
            }
            listOfMovies = NSArray(array: dataArray).sortedArrayUsingDescriptors([
                NSSortDescriptor(key: "year", ascending: false)
                ])
        }
    }
    
    func getDataForMovieById(movie : Movie) -> Void {
        var json = NSDictionary()
        do {
            json = try NSJSONSerialization.JSONObjectWithData(NSData(contentsOfURL: NSURL(string: "http://www.omdbapi.com/?i=\(movie.id)&page=1&plot=full")!)!, options: NSJSONReadingOptions()) as! NSDictionary
        } catch {
            print(error)
        }
        movie.genre = json.objectForKey("Genre") as! String
        movie.plot = json.objectForKey("Plot") as! String
    }
}