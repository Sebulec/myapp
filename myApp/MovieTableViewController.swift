//
//  MovieTableViewController.swift
//  myApp
//
//  Created by Sebastian Kotarski on 19.06.2016.
//  Copyright © 2016 Sebastian Kotarski. All rights reserved.
//

import UIKit

class MovieTableViewController: UITableViewController, UISearchResultsUpdating, UISearchBarDelegate {
    
    
    private var databaseParser : DatabaseParser!
    private var selectedMovie : Movie?
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        databaseParser = DatabaseParser()
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.searchBar.sizeToFit()
        tableView.tableHeaderView = searchController.searchBar
        self.searchController.searchBar.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return databaseParser != nil ? databaseParser.listOfMovies.count : 0
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
        let movie = databaseParser.listOfMovies!.objectAtIndex(indexPath.row) as! Movie
        (cell.viewWithTag(1) as! UIImageView).image! = movie.image!
        (cell.viewWithTag(2) as! UILabel).text? = movie.title
        (cell.viewWithTag(3) as! UILabel).text? = movie.year
        return cell
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        print("Searching: \(searchController.searchBar.text!)")
        let trimmedSearchString = searchController.searchBar.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        let searchString = searchController.searchBar.text!
        if !trimmedSearchString.isEmpty {
            databaseParser.makeRequest(searchString)
            if databaseParser.listOfMovies.count == 0 {
                nothingFoundAlert()
            }
            tableView.reloadData()
        }
        
    }
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        
    }
    
    func nothingFoundAlert() {
        let alertController = UIAlertController(title: "Information", message: "Nothing found", preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alertController.addAction(defaultAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        self.selectedMovie = databaseParser.listOfMovies.objectAtIndex((tableView.indexPathForSelectedRow?.row)!) as? Movie
        let detailView = segue.destinationViewController as! MovieDetailViewController
        detailView.selectedMovie = self.selectedMovie
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            self.databaseParser.getDataForMovieById(self.selectedMovie!)
            dispatch_async(dispatch_get_main_queue(), {
                detailView.selectedMovie = self.selectedMovie
                detailView.movieSubtitle.text = "Genre: \(self.selectedMovie!.genre) \nYear: \(self.selectedMovie!.year)"
                detailView.moviePlot.text = self.selectedMovie!.plot
            });
        }
    }
}

