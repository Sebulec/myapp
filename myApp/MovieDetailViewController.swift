//
//  MovieDetailViewController.swift
//  myApp
//
//  Created by Sebastian Kotarski on 19.06.2016.
//  Copyright © 2016 Sebastian Kotarski. All rights reserved.
//

import UIKit

class MovieDetailViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var movieSubtitle: UILabel!
    @IBOutlet weak var moviePlot: UITextView!
    
    var selectedMovie : Movie!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        print("Selected: \(self.selectedMovie)")
        self.movieTitle?.text = self.selectedMovie.title!
        self.movieSubtitle?.text = "Genre: \(self.selectedMovie.genre!) \nYear: \(self.selectedMovie.year!)"
        self.moviePlot?.text = self.selectedMovie.plot!
        if(self.selectedMovie!.image != nil){
            self.imageView.image = self.selectedMovie!.image!
        }
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeClicked(sender: UIButton?) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func swipedDown(sender: UISwipeGestureRecognizer) {
        closeClicked(nil)
    }
}
