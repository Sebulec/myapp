//
//  Movie.swift
//  myApp
//
//  Created by Sebastian Kotarski on 19.06.2016.
//  Copyright © 2016 Sebastian Kotarski. All rights reserved.
//

import UIKit

class Movie: NSObject {

    var title : String!
    var plot : String!
    var genre : String!
    var posterUrl : String!
    var year : String!
    var id : String!
    var image : UIImage?
    
    init(title: String, plot: String, genre: String, posterUrl: String, year: String, id : String) {
        super.init()
        self.title = title
        self.plot = plot
        self.genre = genre
        self.posterUrl = posterUrl
        self.year = year
        self.id = id
    }
    override var description: String {
        return "title: \(title)" +
            "plot: \(plot)" +
            "genre: \(genre)" +
            "posterUrl: \(posterUrl)" +
            "year: \(year)" +
            "id: \(id)"
    }
    
    func loadImage() {
        self.image = UIImage(data: NSData(contentsOfURL: NSURL(string: posterUrl)!)!)
    }
}
